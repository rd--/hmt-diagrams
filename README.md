hmt-diagrams
------------

[cairo](http://cairographics.org/) &
[html](http://www.w3.org/TR/html5)
rendering of [hmt](http://rohandrape.net/t/hmt)
related diagrams.

## cli

[csv-mnd-draw](?t=hmt-diagrams&e=md/csv-mnd-draw.md)

© [rohan drape](http://rohandrape.net/),
  2006-2024,
  [gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 19  Tried: 19  Errors: 0  Failures: 0
$
```
