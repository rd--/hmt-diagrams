-- | Contour contact sheets.
module Music.Theory.Diagram.Render.Contour.Wt where

import Control.Monad {- base -}
import Text.Printf {- base -}

import qualified Graphics.Rendering.Cairo as C {- cairo -}

import Data.Cg.Minus {- hcg-minus -}
import Render.Cg.Minus {- hcg-minus-cairo -}

data Setup = Setup
  { wt_nc :: Int
  -- ^ number of columns of grid
  , wt_dimensions :: (Int, Int)
  -- ^ (x-divisions,y-divisions)
  , wt_spacers :: (R, R)
  , wt_scalar :: R
  }

-- | cn = contour
type CN = [(Bool, [Int])]

type PP = [CN]

i_to_f :: Integral a => a -> R
i_to_f = fromIntegral

i_pt :: Integral a => a -> a -> Pt R
i_pt x y = Pt (i_to_f x) (i_to_f y)

{- | Generate grid points.
l=left, u=upper, r=rows, c=columns, w=width, h=height
-}
grid :: (R, R) -> (Int, Int) -> (R, R) -> (R, R) -> [Pt R]
grid (l, u) (r, c) (w, h) (dx, dy) =
  let xs = take r [l, l + w + dx ..]
      ys = take c [u, u + h + dy ..]
  in concatMap (\y -> map (`Pt` y) xs) ys

{- | Calculate number of rows (nr) given number of columns (nc) and
number of entries (ne).
-}
calc_nr :: Integral t => t -> t -> t
calc_nr nc ne =
  let (a, b) = ne `divMod` nc
  in a + if b == 0 then 0 else 1

-- | Cairo co-ordinates are /y/ descending.
invert :: (Num a) => a -> [a] -> [a]
invert n = map (n -)

draw_contour :: Pt R -> Bool -> [Int] -> C.Render ()
draw_contour (Pt x y) c ys = do
  let (r, g, b) = if c then (1, 0, 0) else (0, 0, 0)
      p = zipWith i_pt [0 ..] ys
  C.save
  C.setSourceRGBA r g b 0.75
  C.setLineWidth 0.05
  C.translate x y
  line (Ls p)
  C.stroke
  C.restore

draw_border :: Pt R -> (Int, Int) -> C.Render ()
draw_border (Pt x y) (w, h) = do
  let g = 0.75
  C.save
  C.setSourceRGBA g g g 0.75
  C.setLineWidth 0.05
  C.translate x y
  C.rectangle 0 0 (i_to_f w) (i_to_f h)
  C.stroke
  C.restore

draw_img :: Pt R -> (Int, Int) -> CN -> C.Render ()
draw_img p (w, h) xs = do
  draw_border p (w, h)
  mapM_ (\(c, ys) -> draw_contour p c (invert h ys)) xs

draw_wt :: Setup -> PP -> C.Render ()
draw_wt (Setup nc (w, h) sp sc) dd = do
  let nr = calc_nr nc (length dd)
      g = grid (1, 1) (nc, nr) (i_to_f w, i_to_f h) sp
  C.save
  C.scale sc sc
  zipWithM_ (\d p -> draw_img p (w, h) d) dd g
  C.showPage
  C.restore

-- | Select format from extension (ie. @.pdf@ or @.svg@).
draw :: FilePath -> String -> Setup -> [PP] -> IO ()
draw o_fn ext wt_f ddd = do
  let (Setup nc (w, h) (dx, dy) sc) = wt_f
      nr = calc_nr nc (maximum (map length ddd))
      w' = i_to_f nc * (i_to_f w + dx) + dx
      h' = i_to_f nr * (i_to_f h + dy) + dy
      f_multi_page s = C.renderWith s (mapM_ (draw_wt wt_f) ddd)
      f_single_page dd s = C.renderWith s (draw_wt wt_f dd)
      mk_pg_fn k = printf "%s.%02d%s" o_fn k ext
      f_svg (k, dd) = C.withSVGSurface (mk_pg_fn k) (w' * sc) (h' * sc) (f_single_page dd)
      k_ddd = zip [0 :: Int ..] ddd
  case ext of
    ".pdf" -> C.withPDFSurface (o_fn ++ ext) (w' * sc) (h' * sc) f_multi_page
    ".svg" ->
      if length ddd == 1
        then C.withSVGSurface (o_fn ++ ext) (w' * sc) (h' * sc) f_multi_page
        else mapM_ f_svg k_ddd
    _ -> undefined

draw_pdf_and_svg :: FilePath -> Setup -> [PP] -> IO ()
draw_pdf_and_svg o_fn wt_f ddd = do
  draw o_fn ".pdf" wt_f ddd
  draw o_fn ".svg" wt_f ddd
