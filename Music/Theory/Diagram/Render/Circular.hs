-- | Functions for circular representations of Zn structures.
module Music.Theory.Diagram.Render.Circular where

import Control.Monad {- base -}
import Data.Maybe {- base -}

import Data.Colour {- colour -}
import qualified Graphics.Rendering.Cairo as C {- cairo -}

import Data.Cg.Minus {- hcg-minus -}
import qualified Data.Cg.Minus.Colour.Names as Cg {- hcg-minus -}

import qualified Render.Cg.Minus as Render {- hcg-minus-cairo -}

type R = Double
type P = Pt R

-- | lw = line width
lw :: R
lw = 0.25

-- | 'Cg.circle' and 'Cg.pen' and 'C.stroke'.
circle_s :: Ca -> P -> R -> C.Render ()
circle_s c i j = Render.circle i j >> Render.pen lw c ([], 0) >> C.stroke

{- | Location of /n/ marks along distance /m/.

>>> marks 100 10
[0.0,10.0,20.0,30.0,40.0,50.0,60.0,70.0,80.0,90.0]

>>> map (round . (* 180) . (/ pi)) (marks (2 * pi) 12)
[0,30,60,90,120,150,180,210,240,270,300,330]
-}
marks :: R -> Int -> [R]
marks m n = let i = m / fromIntegral n in [0, i .. m - i]

{- | /n/ marks along circumference of circle of radius /r/.

> marks_p 100 12
-}
marks_p :: R -> Int -> [P]
marks_p r n =
  let f i = pt_from_polar (Pt r (i - (pi / 2)))
  in map f (marks (2 * pi) n)

type Text_f = Maybe (Int -> String)

data Opt = Opt
  { opt_use_colour :: Bool
  , opt_colour_seq :: [Ca]
  , opt_draw_mark :: Bool
  , opt_draw_perimeter :: Bool
  , opt_draw_degree :: Bool
  , opt_mark_radius :: R
  , opt_mark_font_size :: R
  }

def_color_seq :: [Ca]
def_color_seq =
  map
    opaque
    [ Cg.colour_lookup_err "Venetian red"
    , Cg.colour_lookup_err "Swedish azure blue"
    , Cg.colour_lookup_err "Candlelight yellow"
    , Cg.colour_lookup_err "Fern green"
    , Cg.colour_lookup_err "Sepia brown"
    ]

opt_default :: Opt
opt_default = Opt False def_color_seq True False False 1.0 6

-- | Frame, circle at @(0,0)@ with radius /r/ and /n/ marks.
frame :: Opt -> R -> Int -> Text_f -> C.Render ()
frame opt r n t_fn = do
  let g = if opt_use_colour opt then 0.15 else 0.0
      c = rgba_to_ca (g, g, g, 1)
      p = marks_p r n
      p' = marks_p (r * 1.1) n
      t_fn' = fromMaybe (const "") t_fn
  when (opt_draw_perimeter opt) (circle_s c (Pt 0 0) r)
  mapM_ (\i -> circle_s c i 1) p
  when
    (opt_draw_mark opt)
    (mapM_ (\(i, j) -> Render.text c i (opt_mark_font_size opt) j) (zip p' (map t_fn' [0 .. n - 1])))
  when
    (opt_draw_degree opt)
    (Render.text c (Pt 0 0) 4 (show n))

circle_polygon :: R -> Int -> Ca -> [Int] -> C.Render ()
circle_polygon r n c x = do
  let p = let m = marks_p r n in map (m !!) x
  Render.line (Ls p)
  Render.pen lw c ([], 0)
  C.stroke

circle_labels :: Opt -> R -> Int -> Ca -> [Int] -> [String] -> C.Render ()
circle_labels opt r n c x ll = do
  let pp = let m = marks_p r n in map (m !!) x
      mp = zipWith (\p0 p1 -> ln_midpoint (Ln p0 p1)) pp (tail pp)
      d p l = Render.text c p (opt_mark_font_size opt) l
  zipWithM_ d mp ll

circle_marks :: Opt -> R -> Int -> Ca -> [Int] -> C.Render ()
circle_marks opt r n c x = do
  let p = marks_p r n
      p' = map (p !!) x
  mapM_ (\i -> circle_s c i (opt_mark_radius opt)) p'

circle_diagram_set :: Opt -> Int -> Text_f -> [[Int]] -> Maybe [[String]] -> C.Render ()
circle_diagram_set opt n t pp lb = do
  frame opt 100 n t
  let f ((p, l), c) = do
        maybe (return ()) (circle_labels opt 100 n c p) l
        circle_polygon 100 n c p
        circle_marks opt 100 n c p
      cc =
        if opt_use_colour opt
          then cycle (opt_colour_seq opt)
          else repeat (rgba_to_ca (0, 0, 0, 1))
      pp' = maybe (zip pp (repeat Nothing)) (zip pp . map Just) lb
  mapM_ f (zip pp' cc)

circle_diagram :: Opt -> Int -> Text_f -> [Int] -> Maybe [String] -> C.Render ()
circle_diagram opt n t p l = circle_diagram_set opt n t [p] (fmap return l)

{- | Variant of 'render_to_file'.

> import qualified Music.Theory.List as T

> let s = map (T.close 1) [[0..11],[0,2..10],[0,3..9],[0,4,8],[0,5,10,3,8,1,6,11,4,9,2,7]]
> to_file Render.F_Svg "/tmp/circular" (circle_diagram_set opt_default 12 (Just show) s Nothing)

> let s = map (T.close 1) [[0,5,6,7],[1,2,3,8],[4,9,10,11]]
> to_file Render.F_Svg "/tmp/circular" (circle_diagram_set opt_default 12 (Just show) s Nothing)

> let s = [0,1,5,6,12,25,29,36,42,48,49,53,0]
> let t = [0,8,16,18,26,34,0]
> let z = map (\i -> map ((`mod` 72) . (+ i)) s) t
> to_file Render.F_Svg "/tmp/circular" (circle_diagram_set opt_default 72 (Just show) z Nothing)

> import Music.Theory.Combinations

> let s = combinations 2 [0,1,3,5,6]
> let i_to_ic n = if n > 6 then 12 - n else n
> let l = map (\[p,q] -> [show (i_to_ic ((p - q) `mod` 12))]) s
> to_file Render.F_Svg "/tmp/circular" (circle_diagram_set opt_default 12 (Just show) s (Just l))
-}
to_file :: Render.File_Type -> FilePath -> C.Render () -> IO ()
to_file ty nm f =
  let f' = C.translate 125 125 >> f
  in Render.render_to_file ty (250, 250) nm f'
