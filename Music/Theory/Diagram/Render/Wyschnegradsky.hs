-- | <http://www.ivan-wyschnegradsky.fr/en/chromatic-drawings/>
module Music.Theory.Diagram.Render.Wyschnegradsky where

import Control.Monad {- base -}
import qualified Graphics.Rendering.Cairo as C {- cairo -}

import qualified Data.Cg.Minus as CG {- hcg-minus -}
import Data.Cg.Minus.Colour (Ca) {- hcg-minus -}
import Data.Cg.Minus.Types {- hcg-minus -}
import qualified Render.Cg.Minus as CG {- hcg-minus-cairo -}

import Music.Theory.Wyschnegradsky {- hmt -}

type R = Double

lw :: R
lw = 0.075

circle_s :: Ca -> Pt R -> R -> C.Render ()
circle_s c i j = CG.circle i j >> CG.pen lw c ([], 0) >> C.stroke

line_s :: Ca -> Pt R -> Pt R -> C.Render ()
line_s c p q = CG.line (Ls [p, q]) >> CG.pen lw c ([], 0) >> C.stroke

annular :: Pt R -> R -> R -> R -> R -> C.Render ()
annular (Pt x y) ir xr sa a =
  let ea = sa + a
      x2 = x + ir * cos sa -- ll
      y2 = y + ir * sin sa
      x3 = x + xr * cos sa -- ul
      y3 = y + xr * sin sa
      x4 = x + ir * cos ea -- lr
      y4 = y + ir * sin ea
  in C.moveTo x2 y2
      >> C.lineTo x3 y3
      >> C.arc x y xr sa ea
      >> C.lineTo x4 y4
      >> C.arcNegative x y ir ea sa

annular_s :: Ca -> Pt R -> R -> R -> R -> R -> C.Render ()
annular_s clr c ir xr sa a = annular c ir xr sa a >> CG.pen lw clr ([], 0) >> C.stroke

annular_f :: Ca -> Pt R -> R -> R -> R -> R -> C.Render ()
annular_f clr c ir xr sa a = annular c ir xr sa a >> CG.colour clr >> C.fill

{- | Marks

>>> map (round . (* 180) . (/ pi)) (marks (2 * pi) 12)
[0,30,60,90,120,150,180,210,240,270,300,330]
-}
marks :: R -> Int -> [R]
marks m n = let i = m / fromIntegral n in [0, i .. m - i]

-- > marks_p 100 (2 * pi) 12
marks_p :: R -> R -> Int -> [Pt R]
marks_p r m n = let f i = CG.pt_from_polar (Pt r (i - (pi / 2))) in map f (marks m n)

grid_cl :: R -> Int -> R -> C.Render ()
grid_cl r c_div r_div = do
  let g = 0.15
      c = CG.rgba_to_ca (g, g, g, 1)
      p = marks_p r (2 * pi) c_div
      r_incr = r / r_div
      r_seq = [r_incr, r_incr * 2 .. r]
  mapM_ (circle_s c (Pt 0 0)) r_seq
  mapM_ (line_s c (Pt 0 0)) p

grid_a :: R -> R -> Int -> Int -> Seq Ca -> C.Render ()
grid_a rot r c_div r_div sq = do
  let clr = seq_group c_div r_div sq
      a_incr = (2 * pi) / fromIntegral c_div
      a_seq = map (+ rot) (marks (2 * pi) c_div)
      r_incr = r / fromIntegral r_div
      r_seq = reverse [0, r_incr .. r - r_incr]
      ring r' = zipWithM_ (\a' c -> annular_f c (Pt 0 0) r' (r' + r_incr) a' a_incr) a_seq
  zipWithM_ ring r_seq clr

grid_rect :: (R, R) -> Int -> Int -> Seq Ca -> C.Render ()
grid_rect (w, h) w_div h_div sq = do
  let clr = seq_group w_div h_div sq
      w_incr = w / fromIntegral w_div
      w_seq = map (subtract (w / 2)) (marks w w_div)
      h_incr = h / fromIntegral h_div
      h_seq = map (subtract (h / 2)) [0, h_incr .. h - h_incr]
      ln y = zipWithM_ (\x c -> CG.rect_fill c (Pt x y) (w_incr, h_incr)) w_seq
  -- C.liftIO (print (w_incr,h_incr,w_seq,h_seq))
  zipWithM_ ln h_seq clr

to_file :: (R, R) -> CG.File_Type -> FilePath -> C.Render () -> IO ()
to_file (w, h) ty nm f =
  let w' = w + 50
      h' = h + 50
      f' = C.translate (w' / 2) (h' / 2) >> f
  in CG.render_to_file ty (w', h') nm f'

iw_clr_seq_6_name :: [String]
iw_clr_seq_6_name = ["Red", "Orange", "Yellow", "Green", "Blue", "Violet"]

iw_clr_seq_6_abbrev :: [Char]
iw_clr_seq_6_abbrev = "ROYGBV"

iw_clr_seq_12_abbrev :: [Char]
iw_clr_seq_12_abbrev = "RrOoYyGgBbVv"

iw_clr_b_hex :: [String]
iw_clr_b_hex =
  [ "#d23017"
  , "#e87f2c"
  , "#db8909"
  , "#f0a724"
  , "#d7bb57"
  , "#e8c954"
  , "#99a330"
  , "#c0bb55"
  , "#4a7379"
  , "#869a98"
  , "#cb6271"
  , "#d19980"
  ]

iw_clr_9_hex :: [String]
iw_clr_9_hex =
  [ "#a22511"
  , "#bf3f24"
  , "#cb6e21"
  , "#da9d4a"
  , "#cfa92a"
  , "#ecce5c"
  , "#334839"
  , "#687034"
  , "#172d44"
  , "#557077"
  , "#7e3a49"
  , "#d18180"
  ]

hex_to_ca :: String -> Ca
hex_to_ca =
  let f = clr_normalise 255 :: (Int, Int, Int) -> (R, R, R)
  in CG.rgb_to_ca . f . parse_hex_clr

iw_clr_b :: [Ca]
iw_clr_b = map hex_to_ca iw_clr_b_hex

iw_clr_9 :: [Ca]
iw_clr_9 = map hex_to_ca iw_clr_9_hex
