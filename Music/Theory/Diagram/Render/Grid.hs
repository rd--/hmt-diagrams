{- | Functions for drawing grid and table structure common in music
theory and in compositions such as Morton Feldman's durational
/grid/ music of the 1950's.
-}
module Music.Theory.Diagram.Render.Grid where

import qualified Graphics.Rendering.Cairo as Cairo {- cairo -}

import Data.Cg.Minus {- hcg-minus -}

import qualified Render.Cg.Minus as Cg {- hcg-minus-cairo -}

import qualified Music.Theory.Diagram.Grid as Grid {- hmt-diagrams -}

-- | R = Real
type R = Double

-- | Red, green, blue & alpha colour (RGBA)
type Clr = (R, R, R, R)

-- | Cell (location,fill-colour,text-colour,text)
type Cell = (Grid.Loc, Clr, Clr, String)

-- | Grid
type Grid = [Cell]

{- | Options:
(cell-dimensions,cell-padding,grid-upper-left,font-size,grid-line-width,grid-line-colour)
-}
type Opt = ((R, R), R, (R, R), R, R, Clr)

{- | Render 'Grid' of /(rows,columns)/ with displacement /(dx,dy)/ in
indicated font size.

> let g = [((0,0),(1,0,0,0.75),(0,0,0,1),"a"),((2,2),(0,0,1,0.75),(0,0,0,1),"b")]
> let opt = ((10,10),0,(20,20),9,0.1,(0.5,0.5,0.5,1))
> Cg.render_to_pdf (80,80) "/tmp/grid.pdf" (mk_grid (4,4) (2,8) opt g)
-}
mk_grid :: (Int, Int) -> (R, R) -> Opt -> Grid -> Cairo.Render ()
mk_grid (r, c) (dx, dy) (cell_dm, cell_pd, grid_ul, fs, g_lw, g_clr) xs = do
  let g = Grid.grid grid_ul cell_dm (r, c)
      grid_fl = uncurry Pt . Grid.displace (cell_pd / 2, cell_pd / 2) . Grid.grid_pt grid_ul cell_dm
      grid_dx = uncurry Pt . Grid.displace (dx, dy) . Grid.grid_pt grid_ul cell_dm
      cell_dm_pd = let (p, q) = cell_dm in (p - cell_pd, q - cell_pd)
      grid_dm = pt_uop fromIntegral (Pt c r) * mk_pt cell_dm
  mapM_ (\(x, y) -> Cg.rect g_lw (rgba_to_ca g_clr) (Pt x y) cell_dm) (concat g)
  mapM_ (\(l, fill_clr, _, _) -> Cg.rect_fill (rgba_to_ca fill_clr) (grid_fl l) cell_dm_pd) xs
  mapM_ (\(l, _, txt_clr, txt) -> Cg.text (rgba_to_ca txt_clr) (grid_dx l) fs txt) xs
  Cg.rect g_lw (rgba_to_ca (0, 0, 0, 1)) (mk_pt grid_ul) (pt_xy grid_dm)
  Cairo.showPage

{- | Write letter grid to SVG file.

> letter_grid_to_svg "/tmp/l.svg" ["AB","CD","ab","cd"]
-}
letter_grid_to_svg :: FilePath -> [[Char]] -> IO ()
letter_grid_to_svg fn gr = do
  let sz = 16
      dx = (4, 12)
      opt = ((sz, sz), 0, (sz, sz), 12, 0.25, (1, 1, 1, 1))
      nr = length gr
      nc = length (head gr)
      mk_cell (i, j) = ((i, j), (1, 1, 1, 1), (0, 0, 0, 1), [(gr !! i) !! j])
      gr' = [mk_cell (i, j) | i <- [0 .. nr - 1], j <- [0 .. nc - 1]]
      i_to_f = fromIntegral
      dm = ((i_to_f nc + 2) * sz, (i_to_f nr + 2) * sz)
  Cg.render_to_svg dm fn (mk_grid (nr, nc) dx opt gr')

{- | Write bit grid to SVG file.

> bit_grid_to_svg "/tmp/b.svg" [[True,False,True],[False,True,False]]
-}
bit_grid_to_svg :: FilePath -> [[Bool]] -> IO ()
bit_grid_to_svg fn gr = do
  let sz = 8
      opt = ((sz, sz), 0, (sz, sz), 9, 0.25, (1, 1, 1, 1))
      nr = length gr
      nc = length (head gr)
      mk_cell (i, j) =
        let clr = if (gr !! i) !! j then (0, 0, 0, 1) else (1, 1, 1, 1)
        in ((i, j), clr, clr, [])
      gr' = [mk_cell (i, j) | i <- [0 .. nr - 1], j <- [0 .. nc - 1]]
      i_to_f = fromIntegral
      dm = ((i_to_f nc + 2) * sz, (i_to_f nr + 2) * sz)
  Cg.render_to_svg dm fn (mk_grid (nr, nc) (0, 0) opt gr')
