{- | Functions to make /path diagrams/ such as those in Fig. VIII-11
on I.Xenakis /Formalized Music/.
-}
module Music.Theory.Diagram.Render.Path where

import Data.Colour {- colour -}
import qualified Graphics.Rendering.Cairo as C {- cairo -}

import Data.Cg.Minus {- hcg-minus -}
import qualified Render.Cg.Minus.Arrow as Cg {- hcg-minus -}

import Music.Theory.Diagram.Path

-- * Drawing

-- | A set of 'Ca' and 'Ls' pairs.
type Path = [(Ca, Ls R)]

-- | Draw 'Path' with mid-point arrows.
draw_path :: Path -> C.Render ()
draw_path xs = do
  mapM_ (uncurry (Cg.arrows_mp 0.1 (pi / 9))) xs
  C.showPage

-- | 'mapM_' 'draw_path'.
draw_paths :: [Path] -> C.Render ()
draw_paths = mapM_ draw_path

-- | 'draw_paths' to named Pdf file.
write_pdf :: FilePath -> [Path] -> IO ()
write_pdf fn xs = do
  let f s =
        C.renderWith
          s
          ( C.translate 10 100
              >> C.scale 100 100
              >> draw_paths xs
          )
  C.withPDFSurface fn 500 500 f

-- * Path diagram

{- | Write Pdf of a set of 'Path_Diagram's to named file.

> let p1 = [(1,4),(2,1),(1,1),(2,2),(2,3),(1,2),(1,3),(2,4)]
> let p2 = [(1,3),(1,2),(2,3),(2,2),(1,1),(2,1),(1,4),(2,4)]
> let p3 = [(1,2),(1,3),(2,4),(1,4),(2,1),(1,1),(2,2),(2,3)]
> path_diagram "/tmp/t.pdf" [p1,p2,p3]
-}
path_diagram :: FilePath -> [Path_Diagram] -> IO ()
path_diagram fn =
  let f (i, j) = (opaque black, Ls [i, j])
  in write_pdf fn . map (map (ln_fn f) . to_unit 4 . mk_path_sm)
