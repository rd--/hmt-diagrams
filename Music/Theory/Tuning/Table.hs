-- | Tuning tables
module Music.Theory.Tuning.Table where

import qualified Text.Html.Minus as H {- html-minus -}

import Music.Theory.Pitch {- hmt -}
import Music.Theory.Tuning.Et {- hmt -}

import qualified Music.Theory.Diagram.Grid as G

-- * Cell

-- | 'G.Table_Cell' from set of 'HS_R'.
hs_r_cell :: (p -> String) -> Int -> (Int -> String) -> [HS_R p] -> (Int, Int) -> G.Table_Cell
hs_r_cell pp n nm_f t (i, j) =
  let t' = nm_f i : hs_r_pp pp n (t !! i)
      e = t' !! j
  in ([], [H.cdata e])

hs_r_pitch_cell :: Int -> (Int -> String) -> [HS_R Pitch] -> (Int, Int) -> G.Table_Cell
hs_r_pitch_cell = hs_r_cell pitch_pp
