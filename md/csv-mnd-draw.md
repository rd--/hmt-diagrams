# csv-mnd-draw

Draw CSV `MND` file using `gnuplot` and write to `SVG` file.

~~~~
$ hmt-csv-mnd-draw --ty=svg --w=1200 --h=200 ~/sw/hmt/csv/mnd/1080-C01.csv ~/sw/hmt-diagrams/data/svg
~~~~

[![](sw/hmt-diagrams/data/svg/1080-C01.svg)](sw/hmt-diagrams/data/svg/1080-C01.svg)
