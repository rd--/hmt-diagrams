import System.FilePath {- filepath -}

import qualified Music.Theory.Opt as T {- hmt-base -}

import qualified Music.Theory.Array.Csv.Midi.Mnd as T {- hmt -}
import qualified Music.Theory.Time.Seq as T {- hmt -}

import qualified Music.Theory.Diagram.Sequencer as D {- hmt-diagrams -}

opt_def :: [T.OptUsr]
opt_def =
  [("h","200","int","image height")
  ,("ty","svg","string","output file-type")
  ,("w","1200","int","image width")
  ,("y0","21","float","y-axix (midi-note-number) min")
  ,("y1","108","float","y-axis (midi-note-number) max")]

load_csv :: FilePath -> IO (T.Wseq Double (T.Event Double))
load_csv = T.csv_midi_read_wseq

-- > let c_fn = "/home/rohan/sw/hmt/csv/mnd/1080-C01.csv"
-- > let c_fn = "/home/rohan/uc/sp-id/csv/music/ngv/s-gyrostasis.plain.csv"
-- > csv_mnd_draw (2400,200) c_fn "/tmp"
csv_mnd_draw :: (Int,Int) -> (Double,Double) -> FilePath -> FilePath -> IO ()
csv_mnd_draw (w,h) (y0,y1) csv_fn svg_dir = do
  sq <- load_csv csv_fn
  let nm = takeBaseName csv_fn
      opt = ([],((w,h),T.wseq_tspan sq,(y0,y1)))
  D.sequencer_plot_midi opt svg_dir nm sq

help :: [String]
help =
  ["hmt-csv-mnd-draw [opt] csv-file output-dir"]

main :: IO ()
main = do
  (o,a) <- T.opt_get_arg True help opt_def
  case a of
    [csv_fn,out_dir] -> csv_mnd_draw
                        (T.opt_read o "w",T.opt_read o "h")
                        (T.opt_read o "y0",T.opt_read o "y1")
                        csv_fn
                        out_dir
    _ -> T.opt_usage help opt_def
