all:
	echo "hmt-diagrams-diagrams"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -Rf dist dist-newstyle *~
	(cd cmd; make clean)

push-all:
	r.gitlab-push.sh hmt-diagrams

indent:
	fourmolu -i Music

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Music/Theory
